/*global define*/
'use strict';

define(['angular','jquery','angularanimate'], function (angular,jquery,angularanimate) {
	return angular.module('todomvc', ['ngAnimate']);
});
