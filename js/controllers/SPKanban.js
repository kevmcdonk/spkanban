/*global define*/
'use strict';

/**
 * The main controller for the app. The controller:
 * - retrieves and persist the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
 */

define(['app', 'services/SPKanbanTaskService'], function (app) {
	return app.controller('SPKanbanController', ['$scope', '$location', 'SPKanbanTaskService', 'filterFilter',
		function SPKanbanController($scope, $location, SPKanbanTaskService, filterFilter) {
			$scope.tasks = [];
			$scope.taskCategories = null;
			$scope.newTask = null;
			$scope.editedTask = null;

			$scope.showLoading = true;
			$scope.showTaskList = true;
			$scope.showTaskDetails = false;

			$scope.moveRight = moveRight;
			$scope.moveLeft = moveLeft;

			loadData();

			function loadData() {
				SPKanbanTaskService.getCategories().done(getTaskCategoriesSucceeded).fail(failed);
				SPKanbanTaskService.get().done(getTasksSucceeded).fail(failed);//.always(refreshView);
			}
			
			function getTasksSucceeded(data) {
				$scope.tasks = data.value;
				$scope.tasks.forEach(function(task){
					task.moving = false;
				});
			}

			function getTaskCategoriesSucceeded(data) {
				$scope.taskCategories = data;
				$scope.showLoading = false;
				$scope.showTaskList = false;
			}

			function putTaskSucceeded(data) {
				$scope.success = data;
				data.moving = false;
			}

			function moveTask(task, direction) {
				if ($scope.taskCategories != null & $scope.taskCategories.length > 0) {
					var catIndex = 0;
					var selectedIndex = -1;
					$scope.taskCategories.forEach(function(category){
						if (task.Status == category) {
							selectedIndex = catIndex;
						}
						catIndex++;
					});

					if (selectedIndex != -1) {
						if (direction == 'left' && selectedIndex != 0) {
							selectedIndex--;
						}
						else if (direction == 'right' && selectedIndex < $scope.taskCategories.length-1) {
							selectedIndex++;
						}
						task.moving = true;
						task.Status = $scope.taskCategories[selectedIndex];
						SPKanbanTaskService.put(task).done(putTaskSucceeded).fail(failed);
					}
				}
			}

			function moveLeft(task) {
				moveTask(task, 'left');
			}

			function moveRight(task) {
				moveTask(task, 'right');	
			}

			function failed(data) {
				alert(data);
			}			
			/*
			$scope.$watch('todos', function () {
				$scope.remainingCount = filterFilter(todos, { completed: false }).length;
				$scope.doneCount = todos.length - $scope.remainingCount;
				$scope.allChecked = !$scope.remainingCount;
				todoStorage.put(todos);
			}, true);

			if ($location.path() === '') {
				$location.path('/');
			}

			$scope.location = $location;

			$scope.$watch('location.path()', function (path) {
				$scope.statusFilter = (path === '/active') ?
					{ completed: false } : (path === '/completed') ?
					{ completed: true } : null;
			});


			$scope.addTodo = function () {
				var newTodo = $scope.newTodo.trim();
				if (!newTodo.length) {
					return;
				}

				todos.push({
					title: newTodo,
					completed: false
				});

				$scope.newTodo = '';
			};


			$scope.editTodo = function (todo) {
				$scope.editedTodo = todo;
			};


			$scope.doneEditing = function (todo) {
				$scope.editedTodo = null;
				todo.title = todo.title.trim();

				if (!todo.title) {
					$scope.removeTodo(todo);
				}
			};


			$scope.removeTodo = function (todo) {
				todos.splice(todos.indexOf(todo), 1);
			};


			$scope.clearDoneTodos = function () {
				$scope.todos = todos = todos.filter(function (val) {
					return !val.completed;
				});
			};


			$scope.markAll = function (done) {
				todos.forEach(function (todo) {
					todo.completed = done;
				});
			};
			*/
		}
	]);
});
