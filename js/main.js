/*global require*/
'use strict';

require.config({
	paths: {
		angular: '../bower_components/angular/angular',
		jquery:  '../bower_components/jquery/dist/jquery',
		angularanimate: '../bower_components/angular-animate/angular-animate'
	},
	shim: {
		angular: {
			exports: 'angular'
		},
		jquery: {
			exports: 'jquery'
		},
		angularanimate: {
			exports: 'angularanimate'
		}
	}
});

require(['angular', 'angularanimate', 'jquery', 'app', 'controllers/SPKanban', 'directives/todoFocus'], function (angular,jquery) {
	angular.bootstrap(document, ['todomvc']);
});