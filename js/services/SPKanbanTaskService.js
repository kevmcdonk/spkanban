/*global define*/
'use strict';

/**
 * Services that persists and retrieves tasks from SharePoint list.
 */
define(['app','jquery'], function (app,jquery) {
	app.factory('SPKanbanTaskService', function ($http) {
		
		return {
			get: function () {
				var deferred = jquery.Deferred();

            	$http.get('https://mcdonnell.sharepoint.com/SPKanban/_vti_bin/client.svc/lists/getByTitle(\'SPKanbanTasks\')/items').
				  success(function(data, status, headers, config) {
				    // this callback will be called asynchronously
				    // when the response is available
				  	deferred.resolve(data);
				  }).
				  error(function(data, status, headers, config) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				    deferred.reject("ERROR");
				  });

            return deferred.promise();
			},
			getCategories: function () {
				var deferred = jquery.Deferred();

            	$http.get('https://mcdonnell.sharepoint.com/SPKanban/_vti_bin/client.svc/lists/getByTitle(\'SPKanbanTasks\')/fields/getbytitle(\'Task Status\')').
				  success(function(data, status, headers, config) {
				    // this callback will be called asynchronously
				    // when the response is available
				    
				  	deferred.resolve(data.Choices);
				  }).
				  error(function(data, status, headers, config) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				    deferred.reject("ERROR");
				  });

            return deferred.promise();
			},

			put: function (task) {
				var deferred = jquery.Deferred();
					
					var listName = "SPKanbanTasks";
				    var itemType = "SP.Data." + listName.charAt(0).toUpperCase() + listName.slice(1) + "ListItem";
				    var item = {
				        "__metadata": { "type": itemType },
				        "Title": task.Title,
				        "Status": task.Status,
				        "Priority": task.Priority,
				        "PercentComplete": task.PercentComplete,
				        "AssignedTo": task.AssignedTo,
				        "Body": task.Body,
				        "StartDate": task.StartDate,
				        "DueDate": task.DueDate
				    };
				    task.__metadata = {"type": itemType};

				    var taskJSON = JSON.stringify(item);
				    var requestDigest = $("#__REQUESTDIGEST").val();

				    $.ajax({
				        url: "https://mcdonnell.sharepoint.com/SPKanban/_api/web/lists/getbytitle('" + listName + "')/items(" + task.ID + ")",
				        type: "POST",
				        contentType: "application/json;odata=verbose",
				        data: taskJSON,
				        headers: {
				            "Accept": "application/json;odata=verbose",
				            "X-HTTP-Method":"MERGE",
				            "IF-MATCH": "*",
				            "X-RequestDigest": requestDigest
				        },
				        success: function (data) {
				            deferred.resolve(task);
				        },
				        error: function (data) {
				            deferred.reject("ERROR");
				        }
				    });

				/*
				jQuery.ajax({
			        url: "https://mcdonnell.sharepoint.com/SPKanban/_api/web/lists/GetByTitle('SPKanbanTasks')/items(" + task.ID + ")",
			        type: "POST",
			        data: JSON.stringify({ '__metadata': { 'type': 'SP.Data.TestListItem' }, 'Title': 'TestUpdated' }),
			        headers: { 
			           Authorization: "Bearer " + accessToken
     X-RequestDigest: form digest value
    "IF-MATCH": etag or "*"
    "X-HTTP-Method":"MERGE",
    accept: "application/json;odata=verbose"
    content-type: "application/json;odata=verbose"
    content-length:length of post body
			        },
			        success: doSuccess,
			        error: doError
				});
				*/
				/*
				$http({method: 'PUSH', url: 'https://mcdonnell.sharepoint.com/SPKanban/_vti_bin/client.svc/lists/getByTitle(\'SPKanbanTasks\')' ///items(' + task.ID + ')'
					, headers: {'X-RequestDigest': $("#__REQUESTDIGEST").val()
					, data: task}
				}).
				  success(function(data, status, headers, config) {
				    // this callback will be called asynchronously
				    // when the response is available
				    
				  	deferred.resolve(data);
				  }).
				  error(function(data, status, headers, config) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				    deferred.reject("ERROR");
				  });
				 */
				/*
            	$http.put(', task).
				  success(function(data, status, headers, config) {
				    // this callback will be called asynchronously
				    // when the response is available
				    
				  	deferred.resolve(data);
				  }).
				  error(function(data, status, headers, config) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				    deferred.reject("ERROR");
				  });
					*/
            return deferred.promise();
			}


		};
	});
});
